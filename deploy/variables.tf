variable "prefix" {
  # recipe app api DevOps
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "recipe-app-api-devops@dontreveal.net"
}